$(document).ready(function() {
    $('.login__phone-control').inputmask('+7 999 999 99 99', {
        showMaskOnHover: false,
    });

    /* Регистрация */
    $('.login__reg-form').validate({
        submitHandler: function(form) {
            $('html').addClass('is-waiting');

            $.ajax({
                type: "POST",
                url: $(form).attr('action'),
                data: $(form).serialize()
            }).done(function(data) {
                $('html').removeClass('is-waiting');

                if(data.error){
                    customAlert({
                        icon: 'error',
                        text: 'Никнейм занят',
                        button: true,
                        buttonText: 'Новый никнейм',
                        buttonClass: 'popup-window__remove-btn',
                    });
                } else {
                    var name = $('.login__name-control').val();
                    var phone = $('.login__phone-control').val();
                    setSMS(name, phone);
                    
                    $('.login__section_reg').hide();
                    $('.login__section_sms').fadeIn();

                    $('.login__sms-title-phone').text($('.login__phone-control').val());
                    $('.login__sms-control').focus();
                }
            });
        }
    });

    /* Повторить смс код */
    $('.login__sms-repeat-btn').on('click', function(e){
        e.preventDefault();
        
        var name = $('.login__name-control').val();
        var phone = $('.login__phone-control').val();
        setSMS(name, phone);
    });

    /* Подтверждение смс кода */
    $('.login__sms-form').validate({
        submitHandler: function(form) {
            $('html').addClass('is-waiting');

            $.ajax({
                type: "POST",
                url: $(form).attr('action'),
                data: $(form).serialize()
            }).done(function(data) {
                $('html').removeClass('is-waiting');

                if(data.error){
                    customAlert({
                        icon: 'error',
                        text: 'Неправильный код',
                        button: true,
                        buttonText: 'Новый код',
                        buttonClass: 'popup-window__remove-btn',
                    });
                } else {
                    window.location.href = "game.php";
                }
            });
        }
    });
});

$.validator.addMethod('login__phone-control', function(value, element) {
    return this.optional(element) || value.match(/\+7\s[0-9]{3}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/);
}, "&nbsp;");

var repeatSmsInterval;

function setSMS(name, phone){
    /* Отправляем смс код */
    /*
    $.ajax({
        type: "POST",
        url: "",
        data: '{"name" : name, "phone" : phone}'
    })
    */

    clearInterval(repeatSmsInterval);

    $('.login__sms-timer-row').show();
    $('.login__sms-repeat-btn-row').hide();

    var repeatSmsTime = 60;
    $('.login__sms-timer-value').text(repeatSmsTime);

    repeatSmsInterval = setInterval(function(){
        if(repeatSmsTime == 0){
            clearInterval(repeatSmsInterval);

            $('.login__sms-timer-row').hide();
            $('.login__sms-repeat-btn-row').show();
        } else {
            $('.login__sms-timer-value').text(repeatSmsTime);
            repeatSmsTime--;
        }
    }, 1000);
}