function isDesktop(){
    return $(window).outerWidth() > 1023;
}

function isMobile(){
    return $(window).outerWidth() < 768;
}

var noscrollY = 0;

function noscrollStart(){
    noscrollY = $(document).scrollTop();
    $('body').css('top', - noscrollY + 'px');
    $('html').addClass('is-noscroll');
}

function noscrollFinish(){
    $('html').removeClass('is-noscroll');
    $(document).scrollTop(noscrollY);
    $('body').css('top', 'auto');
}

function loaderWindow(){
    var loaderTimeout = 0;
    var loaderFinishDelay = 0;

    // Если есть лоадер
    if($('.loader').length){
        loaderTimeout = 3000;
    
        // Если финальный экран на десктопе
        if($('html').hasClass('is-finish') && isDesktop()){
            loaderFinishDelay = 3000;
            
            $('html').addClass('is-finish-loading');
        }

        // Смена текста на лоадере
        setTimeout(function(){
            $('.loader__steps-item').eq(0).addClass('is-active');
            
            setTimeout(function(){
                $('.loader__steps-item').eq(1).addClass('is-active');
                $('.loader__text span').removeClass('is-active').eq(1).addClass('is-active');

                setTimeout(function(){
                    $('.loader__steps-item').eq(2).addClass('is-active');
                    $('.loader__text span').removeClass('is-active').eq(2).addClass('is-active');
                }, loaderTimeout / 3.5);
            }, loaderTimeout / 3.5);
        }, loaderFinishDelay);
        
        // Шкала лоадера
        $('.loader__loader > span').css({
            "animation-duration": loaderTimeout / 1000 + "s",
            "animation-delay": loaderFinishDelay / 1000 + "s",
        });
    }

    // Скрываем лоадер
    setTimeout(function(){
        $('html').removeClass('is-loading');
        
        // Окно куки
        if(!cookieAccept){
            setTimeout(function(){
                $('html').addClass('is-cookie-alert-open');
            }, 1000);
        }
    }, loaderFinishDelay + loaderTimeout);
}

function prizesSlider(){
    var $prizesSlider = $('.prizes-slider');
    
    if($prizesSlider.length){
        if(isMobile()){
            if($prizesSlider.hasClass('slick-slider')){
                $prizesSlider.slick('unslick');
            }
        } else {
            if(!$prizesSlider.hasClass('slick-slider')){
                $prizesSlider.slick({
                    slidesToShow: 3,
                    swipeToSlide: true,
                    touchThreshold: 1000,
                    speed: 1000,
                    focusOnSelect: true,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 1,
                                arrows: false,
                                centerMode: true,
                                centerPadding: '25%',
                            }
                        }
                    ]
                });
            }
        }
    }
}

function customAlert(popup){
    var $popup;

    if(typeof popup === 'object'){
        $popup = $('<div class="popup-window _dynamic' + (popup.closeBtn ? ' _closable' : '') + '">' +
            '<div class="popup-window__content scrollbar">' +
                '<div class="popup' + (popup.size ? (' popup_size_' + popup.size) : '') + '' + (popup.theme ? (' popup_theme_' + popup.theme) : '') + '">' +
                    (popup.closeBtn ? '<div class="popup__close-btn close-btn"></div>' : '') +
                    '<div class="popup__content">' +
                        '<div class="popup__content-inner">' +
                            (popup.icon ? '<div class="popup__icon popup__icon_'+ popup.icon +'"></div>' : '') +
                            (popup.title ? '<div class="popup__title">' + popup.title + '</div>' : '') +
                            (popup.text ? '<div class="popup__text">' + popup.text + '</div>' : '') +
                            (popup.button ? '<div class="popup__btn-wrapper"><button type="button" class="btn' + (popup.theme ? (' btn_' + popup.theme) : '') + (popup.buttonClass ? (' ' + popup.buttonClass) : '') + '"><span><span>' + popup.buttonText + '</span></span></button></div>' : '') +
                        '</div>' +
                    '</div>' +
                    '<div class="popup__dec"><span></span><span><span></span><span></span><span></span><span></span></span><span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span><span><span></span><span></span></span></div>' +
                '</div>' +
            '</div>' +
        '</div>');

        $popup.appendTo('body');
    } else {
        $popup = $(popup);
    }

    noscrollStart();
    
    setTimeout(function(){
        $popup.addClass('is-open');
    });
}

function preloadImages() {
    for (i = 0; i < preloadImages.arguments.length; i++) {
        preloadImagesList[i] = new Image();
        preloadImagesList[i].src = preloadImages.arguments[i];
    }
}

var observer = new IntersectionObserver( 
    ([e]) => e.target.classList.toggle('is-stuck', e.intersectionRatio < 1 && e.intersectionRect.top < 50),
    { threshold: [1] }
)

function plural(num, one, two, five) {
    var n = Math.abs(num);
    
    n %= 100;
    if (n >= 5 && n <= 20) return five;
    
    n %= 10;
    if (n === 1) return one;
    
    if (n >= 2 && n <= 4) return two;
    
    return five;
}