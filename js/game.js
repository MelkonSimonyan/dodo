$(document).ready(function(){
    if(!gameData.userName){
        window.location.href = "index.php";
    } else {
        if(gameData.finish){
            game.finish();
        } else {
            $('.game-score__points').text(gameData.globalScore);
            game.getCurrentMission();
        }
    }

    $('html').on('menu-open', function(){
        game.timerPause = true;
    });

    $('html').on('menu-close', function(){
        game.timerPause = false;
    });
});

var game = {
    timerPause: false,
    getCurrentMission: function(){
        $('html').addClass('is-waiting');

        $.ajax({
            url: 'missions/mission-'+ gameData.currentMission +'.php'
        }).done(function(data) {
            $('.game-missions').html(data);

            setTimeout(function(){
                $('html').removeClass('is-waiting');
            });

            game.mission[gameData.currentMission].start();
        });
    },
    setData: function(){
        /* Отправляем игровую информацию на сервер */
        console.log(gameData);
        
        $.ajax({
            type: "POST",
            url: "/",
            data: gameData
        });
    },
    finish: function(){
        window.location.href = "index.php?finish";
    },
    mission: {
        1: {
            timer: null,
            imageRotateInterval: null,
            imageRotateStart: true,
            imageRotate: function(){
                var imagesLoadCounter = 0;

                $('.mission-1__start-coffee img').each(function(){
                    if(this.complete){
                        incrementLoadCounter();
                    } else {
                        this.addEventListener('load', incrementLoadCounter, false);
                    }
                });

                function incrementLoadCounter() {
                    imagesLoadCounter++;
                    
                    if (imagesLoadCounter === $('.mission-1__start-coffee img').length && game.mission['1'].imageRotateStart) {
                        game.mission['1'].imageRotateInterval = setInterval(function(){
                            var $active = $('.mission-1__start-coffee img.is-active');
                            var $next = $active.next('img').length ? $active.next('img') : $('.mission-1__start-coffee img').eq(0);

                            $active.removeClass('is-active');
                            $next.addClass('is-active');
                        }, 50);
                    }
                }
            },
            start: function(){
                $('.game-location__timer').text('0:' + ('0' + (isDesktop() ? 30 : 15)).slice(-2));
                $('.game-help_timer-value').text(isDesktop() ? 30 : 15);

                game.mission['1'].imageRotate();

                var locationHelper = true;

                $('.mission-1__start-btn').on('click', function(){
                    $('.mission-1').attr('data-status', 'map');

                    game.mission['1'].imageRotateStart = false;
                    clearInterval(game.mission['1'].imageRotateInterval);
                });

                $('.game-map__marker_location').on('click', function(){
                    if(!$(this).hasClass('is-completed') && $('.mission-1').attr('data-status') == 'map'){
                        $(this).addClass('is-completed');

                        $('.mission-1').attr('data-status', $(this).data('location'));
                        
                        if(locationHelper){
                            locationHelper = false;
                            $('.game').attr('data-help', 'score');
                        } else {
                            $('.game').attr('data-help', 'start');
                        }
                    }
                });

                $('.mission-1 .game-help_score .game-help__btn').on('click', function(){
                    $('.game').attr('data-help', 'timer');
                });

                $('.mission-1 .game-help_timer .game-help__btn').on('click', function(){
                    $('.game').attr('data-help', 'scale');
                });

                $('.mission-1 .game-help_scale .game-help__btn').on('click', function(){
                    $('.game').attr('data-help', 'start');
                });

                $('.mission-1 .game-help_start .game-help__btn').on('click', function(){
                    $('.game').removeAttr('data-help');

                    clearInterval(game.mission['1'].timer);

                    var timerValue = isDesktop() ? 30 : 15;
                    $('.game-location__timer').text('0:' + ('0' + timerValue).slice(-2));

                    game.mission['1'].timer = setInterval(function(){
                        if(!game.timerPause){
                            timerValue--;

                            $('.game-location__timer').text('0:' + ('0' + timerValue).slice(-2));

                            if(timerValue == 0){
                                clearInterval(game.mission['1'].timer);
                                game.mission['1'].timer = null;

                                game.mission['1'].timeOut();
                            }
                        }
                    }, 1000);
                });

                $('.game-location__marker').each(function(){
                    $(this).css({
                        left: Math.floor(Math.random() * 101) + '%',
                        top: Math.floor(Math.random() * 101) + '%',
                    });
                });

                $('.game-location__marker').on('click', function(){
                    if(game.mission['1'].timer){
                        var $marker = $(this),
                        $location = $marker.closest('.game-location'),
                        $markersArea = $location.find('.game-location__markers'),
                        $scaleActiveItem = $location.find('.game-location__scale-item.is-active + .game-location__scale-item:not(.is-active)');
                        
                        if(!$scaleActiveItem.length)
                            $scaleActiveItem = $location.find('.game-location__scale-item').eq(0);

                        $marker.addClass('is-active');
                        $scaleActiveItem.addClass('is-active');

                        gameData.mission["1"].score += 1;
                        gameData.globalScore += 1;
                        $('.game-score__points').text(gameData.globalScore);

                        setTimeout(function(){
                            $marker.animate({
                                left: $scaleActiveItem.offset().left + $scaleActiveItem.width() / 2 - $markersArea.offset().left,
                                top:  $scaleActiveItem.offset().top + $scaleActiveItem.height() / 2 - $markersArea.offset().top,
                                width: $scaleActiveItem.width(),
                                height: $scaleActiveItem.height()
                            }, 500, function(){
                                $marker.remove();
                            });
                        }, 250);
                    }
                });

                $(document).on('click', '.mission-1__next-btn', function(){
                    $('.mission-1').attr('data-status', 'map');
                });

                $(document).on('click', '.mission-1__finish-btn', function(){
                    game.mission['1'].finish();
                });
            },
            timeOut: function(){
                var popupTheme = '', popupTitle = '', popupText = '';

                if($('.game-map__marker_location:not(.is-completed)').length){
                    var btnClass = "mission-1__next-btn";

                    if(gameData.mission["1"].score == 0){
                        popupTitle = 'Упс!';
                        popupText = 'Пока не удалось найти стаканчиков, нужно искать быстрее!';
                    } else {
                        popupTheme = 'blue';
                        popupTitle = 'Ура!';
                        popupText = 'Начало положено — у тебя уже '+ gameData.globalScore +' баллов!<br> Но нужно ещё немного взбодриться!';
                    }
                } else {
                    var btnClass = "mission-1__finish-btn";

                    if(gameData.globalScore == 0){
                        popupTitle = 'Ауч!';
                        popupText = 'Время вышло — а стаканчиков нет. Нужно максимально собраться, чтобы выжить дальше!';
                    } else {
                        popupTheme = 'blue';
                        popupTitle = 'Ура!';
                        popupText = 'У тебя уже '+ gameData.globalScore +' баллов, ты бодр и теперь можно смело переходить на следующий уровень!';
                    }
                }

                customAlert({
                    size: 'lg',
                    theme: popupTheme,
                    title: popupTitle,
                    text: popupText,
                    button: true,
                    buttonText: 'ДАЛЕЕ',
                    buttonClass: 'popup-window__remove-btn ' + btnClass,
                });
            },
            finish: function(){
                gameData.currentMission = 2;

                game.setData();
                
                game.getCurrentMission();
            }
        },
        2: {
            timer: null,
            start: function(){
                $('.game-lives__count').text(gameData.mission['2'].lives);

                /* Если игра уже начата */
                if(gameData.mission['2'].currentQuestion != 1){
                    $('.mission-2').attr('data-status', 'test');

                    $('.game').removeAttr('data-help');

                    if(gameData.mission['2'].lives){
                        game.mission['2'].showQuestion();
                    } else {
                        $('.game-test__item').removeClass('is-active');
                        $('.game-test__item[data-id="'+ (gameData.mission['2'].currentQuestion - 1) +'"]').addClass('is-active');

                        if(gameData.mission['2'].friendRequest){
                            game.mission['2'].showFriendRequestPopup();
                        } else {
                            game.mission['2'].showLostLivesPopup();
                        }
                    }
                }

                $('.mission-2__play-btn').on('click', function(){
                    $('.mission-2').attr('data-status', 'video');
                    $('.mission-2-video-window').addClass('is-open');
                    $('.mission-2-video-window video')[0].play();
                    $('.mission-2-video-window video')[0].onended = function(){
                        $('.mission-2-video-window').removeClass('is-open').find('video')[0].pause();

                        $('.mission-2').attr('data-status', 'test');

                        $('.game').attr('data-help', 'lives');
                    };
                });

                $('.mission-2 .game-help_lives .game-help__btn').on('click', function(){
                    $('.game').attr('data-help', 'timer');
                });

                $('.mission-2 .game-help_timer .game-help__btn').on('click', function(){
                    $('.game').removeAttr('data-help');

                    game.mission['2'].showQuestion();
                });

                $('.game-test__item-answer').on('click', function(){
                    if(game.mission['2'].timer){
                        clearInterval(game.mission['2'].timer);
                        game.mission['2'].timer = null;

                        $(this).addClass('is-selected');

                        var currentQuestion = gameData.mission['2'].currentQuestion;

                        if(test[currentQuestion].rightAnswer == $(this).index() + 1 || test[currentQuestion].rightAnswer == 0){
                            /* Правильный ответ */
                            game.mission['2'].rightAnswer();
                            
                            if(test[currentQuestion].rightAnswer == 0){
                                if($(this).index() == 0){
                                    var popupText = test[currentQuestion].successMessage;
                                } else {
                                    var popupText = test[currentQuestion].errorMessage;
                                }
                            } else {
                                var popupText = test[currentQuestion].successMessage;
                            }

                            customAlert({
                                theme: 'blue',
                                icon: 'success',
                                text: popupText,
                                button: true,
                                buttonText: 'ДАЛЕЕ',
                                buttonClass: 'popup-window__remove-btn game-test__next-btn',
                            });
                        } else {
                            /* Неравильный ответ */
                            game.mission['2'].wrongAnswer();

                            customAlert({
                                icon: 'error',
                                text: test[currentQuestion].errorMessage,
                                button: true,
                                buttonText: 'ДАЛЕЕ',
                                buttonClass: 'popup-window__remove-btn game-test__next-btn',
                            });
                        }
                    }
                });

                $(document).on('click', '.game-test__next-btn', function(){
                    if(gameData.mission['2'].lives){
                        game.mission['2'].showQuestion();
                    } else {
                        game.mission['2'].showLostLivesPopup();
                    }
                });

                $(document).on('click', '.game-test__friend-request-btn', function(){
                    gameData.mission['2'].friendRequest = true;
                    game.setData();

                    game.mission['2'].showFriendRequestPopup();
                });

                $(document).on('click', '.game-test__friend-submit-btn', function(){
                    $('html').addClass('is-waiting');

                    /* Проверка друга */
                    $.ajax({
                        url: '/'
                    }).done(function(data) {
                        $('html').removeClass('is-waiting');
                        
                        if(data){
                            $('.popup-window.is-open').removeClass('is-open');
                            
                            noscrollFinish();

                            gameData.mission['2'].friendRequest = false;
                            gameData.mission['2'].lives++;
                            $('.game-lives__count').text(gameData.mission['2'].lives);

                            game.setData();
                            
                            game.mission['2'].showQuestion();
                        }
                    });
                });

                $(document).on('click', '.game-test__10-points-btn', function(){
                    if(gameData.globalScore >= 10){
                        gameData.mission['2'].score -= 10;
                        gameData.globalScore -= 10;
                        $('.game-score__points').text(gameData.globalScore);

                        gameData.mission['2'].friendRequest = false;
                        gameData.mission['2'].lives++;
                        $('.game-lives__count').text(gameData.mission['2'].lives);

                        game.setData();

                        game.mission['2'].showQuestion();
                    }
                });
            },
            showQuestion: function(){
                var $currentQuestion = $('.game-test__item[data-id="'+ gameData.mission['2'].currentQuestion +'"]');

                if($currentQuestion.length){
                    $('.game-test__item').removeClass('is-active');
                    $currentQuestion.addClass('is-active');

                    clearInterval(game.mission['2'].timer);

                    var timerValue = 15;
                    $('.game-test__timer').text('0:' + ('0' + timerValue).slice(-2));

                    game.mission['2'].timer = setInterval(function(){
                        if(!game.timerPause){
                            timerValue--;

                            $('.game-test__timer').text('0:' + ('0' + timerValue).slice(-2));

                            if(timerValue == 0){
                                clearInterval(game.mission['2'].timer);
                                game.mission['2'].timer = null;

                                game.mission['2'].timeOut();
                            }
                        }
                    }, 1000);
                } else {
                    game.mission['2'].finish();
                }
            },
            timeOut: function(){
                game.mission['2'].wrongAnswer();

                customAlert({
                    title: 'Ауч!',
                    text: 'Время вышло. Нужно максимально собраться, чтобы выжить дальше!',
                    button: true,
                    buttonText: 'ДАЛЕЕ',
                    buttonClass: 'popup-window__remove-btn game-test__next-btn',
                });
            },
            rightAnswer: function(){
                gameData.mission['2'].currentQuestion++;
                gameData.mission['2'].score += 1;
                gameData.globalScore += 1;
                $('.game-score__points').text(gameData.globalScore);
                
                game.setData();
            },
            wrongAnswer: function(){
                gameData.mission['2'].currentQuestion++;
                gameData.mission['2'].lives--;
                $('.game-lives__count').text(gameData.mission['2'].lives);

                game.setData();
            },
            showLostLivesPopup(){
                customAlert('#lost-lives-popup');

                if(gameData.globalScore < 10){
                    $('.game-test__10-points-btn').prop('disabled', true);
                } else {
                    $('.game-test__10-points-btn').prop('disabled', false);
                }
            },
            showFriendRequestPopup(){
                customAlert('#friend-request-popup');

                if(gameData.globalScore < 10){
                    $('.game-test__10-points-btn').prop('disabled', true);
                } else {
                    $('.game-test__10-points-btn').prop('disabled', false);
                }
            },
            finish: function(){
                gameData.currentMission = 3;

                game.setData();
                
                game.getCurrentMission();
            }
        },
        3: {
            start: function(){
                if(gameData.mission['3'].promo){
                    $('.mission-3').attr('data-status', 'win');
                    $('.mission-3__promo').text(gameData.mission['3'].promo);
                }

                $('.mission-3__form').validate({
                    submitHandler: function(form) {
                        $('html').addClass('is-waiting');

                        $.ajax({
                            type: "POST",
                            url: $(form).attr('action'),
                            data: $(form).serialize()
                        }).done(function(data) {
                            $('html').removeClass('is-waiting');

                            if(data.error){
                                customAlert({
                                    icon: 'error',
                                    text: 'Неправильный код',
                                    button: true,
                                    buttonText: 'ДАЛЕЕ',
                                    buttonClass: 'popup-window__remove-btn',
                                });
                            } else {
                                if(!gameData.mission['3'].promo){
                                    var promo = 'dodo2285'; // <- Заменить на реальный код

                                    gameData.mission['3'].promo = promo;
                                    gameData.mission['3'].score += 20;
                                    gameData.globalScore += 20;
                                    $('.game-score__points').text(gameData.globalScore);

                                    game.setData();
                                    
                                    $('.mission-3').attr('data-status', 'win');
                                    $('.mission-3__promo').text(promo);
                                }
                            }
                        });
                    }
                });

                $('.mission-3__finish-btn').on('click', function(){
                    if(gameData.mission['3'].promo){
                        game.mission['3'].finish();
                    }
                });
            },
            finish: function(){
                gameData.currentMission = 4;

                game.setData();
                
                game.getCurrentMission();
            }
        },
        4: {
            timer: null,
            speedometerValue: 0,
            start: function(){
                $('.game-lives__count').text(gameData.mission['4'].lives);

                /* Если игра уже начата */
                if(gameData.mission['4'].lives < 3){
                    $('.mission-4').attr('data-status', 'game');

                    if(gameData.mission['4'].lives){
                        $('.game').attr('data-help', 'speed');
                    } else {
                        if(gameData.mission['4'].friendRequest){
                            game.mission['4'].showFriendRequestPopup();
                        } else {
                            game.mission['4'].showLostLivesPopup();
                        }
                    }
                }

                $('.mission-4__start-btn').on('click', function(){
                    $('.mission-4').attr('data-status', 'game');

                    $('.game').attr('data-help', 'speed');
                });

                $('.mission-4 .game-help_speed .game-help__btn').on('click', function(){
                    $('.game').removeAttr('data-help');

                    game.mission['4'].startSpeed();
                });

                $('.game-speed__circle-btn').on('mousedown', function(){
                    if(game.mission['4'].timer){
                        game.mission['4'].stopSpeed();

                        if(game.mission['4'].speedometerValue >= 140){
                            gameData.currentMission = 5;

                            gameData.mission['5'].promo = "FREECITY"; // <- Заменить на реальный код

                            gameData.mission['4'].score += 20;
                            gameData.globalScore += 20;
                            $('.game-score__points').text(gameData.globalScore);
                            
                            game.setData();

                            setTimeout(function(){
                                $('.mission-4-victory-video-window').addClass('is-open');
                                $('.mission-4-victory-video-window video')[0].play();
                                $('.mission-4-victory-video-window video')[0].onended = function(){
                                    $('.mission-4-victory-video-window').find('video')[0].pause();
                                    
                                    customAlert({
                                        size: 'lg',
                                        theme: 'blue',
                                        title: 'Ура!',
                                        text: 'Вы улетаете навстречу победе. За смелость вы получаете шанс выиграть брэндированные призы от Додо Пиццы',
                                        button: true,
                                        buttonText: 'ДАЛЕЕ',
                                        buttonClass: 'popup-window__remove-btn game-speed__finish-btn',
                                    });
                                };
                            },200);
                        } else {
                            gameData.mission['4'].lives--;
                            $('.game-lives__count').text(gameData.mission['4'].lives);

                            game.setData();

                            setTimeout(function(){
                                $('.mission-4-losing-video-window').addClass('is-open');
                                $('.mission-4-losing-video-window video')[0].play();
                                $('.mission-4-losing-video-window video')[0].onended = function(){
                                    $('.mission-4-losing-video-window').removeClass('is-open').find('video')[0].pause();
                                    
                                    if(gameData.mission['4'].lives){
                                        customAlert({
                                            size: 'lg',
                                            title: 'упс!',
                                            text: 'Вы промахнулись, но не унывайте,<br> у вас есть еще ' + (gameData.mission['4'].lives == 2 ? 'две попытки!' : 'одна попытка!'),
                                            button: true,
                                            buttonText: 'ДАЛЕЕ',
                                            buttonClass: 'popup-window__remove-btn game-speed__next-btn',
                                        });
                                    } else {
                                        game.mission['4'].showLostLivesPopup();
                                    }
                                };
                            },200);
                        }
                    }
                });

                $(document).on('click', '.game-speed__next-btn', function(){
                    if(gameData.mission['4'].lives){
                        game.mission['4'].startSpeed();
                    } else {
                        game.mission['4'].showLostLivesPopup();
                    }
                });

                $(document).on('click', '.game-speed__finish-btn', function(){
                    game.mission['4'].finish();
                });

                $(document).on('click', '.game-speed__friend-request-btn', function(){
                    gameData.mission['4'].friendRequest = true;
                    game.setData();

                    game.mission['4'].showFriendRequestPopup();
                });

                $(document).on('click', '.game-speed__friend-submit-btn', function(){
                    $('html').addClass('is-waiting');

                    /* Проверка друга */
                    $.ajax({
                        url: '/'
                    }).done(function(data) {
                        $('html').removeClass('is-waiting');
                        
                        if(data){
                            $('.popup-window.is-open').removeClass('is-open');
                            
                            noscrollFinish();

                            gameData.mission['4'].friendRequest = false;
                            gameData.mission['4'].lives++;
                            $('.game-lives__count').text(gameData.mission['4'].lives);

                            game.setData();
                            
                            game.mission['4'].startSpeed();
                        }
                    });
                });

                $(document).on('click', '.game-speed__10-points-btn', function(){
                    if(gameData.globalScore >= 10){
                        gameData.mission['4'].score -= 10;
                        gameData.globalScore -= 10;
                        $('.game-score__points').text(gameData.globalScore);

                        gameData.mission['4'].friendRequest = false;
                        gameData.mission['4'].lives++;
                        $('.game-lives__count').text(gameData.mission['4'].lives);

                        game.setData();

                        game.mission['4'].startSpeed();
                    }
                });
            },
            startSpeed: function(){
                var C = game.mission['4'].speedometerValue;
                var N = Math.floor(Math.random() * 161);
                var S = 2;
                var $path = $('.game-speed__circle-line path');
                var direction;
                var counter = 0;

                game.mission['4'].timer = setInterval(function(){
                    if(C > N){
                        var newDirection = 'down';
                        game.mission['4'].speedometerValue -= S;
                    } else {
                        var newDirection = 'up';
                        game.mission['4'].speedometerValue += S;
                    }

                    if(direction != newDirection){
                        direction = newDirection;
                        counter++;
                    }
                    
                    $path.css({
                        "stroke-dashoffset" : 946 - game.mission['4'].speedometerValue * 946 / 160
                    });
                    
                    if(Math.abs(game.mission['4'].speedometerValue - N) <= S){
                        C = game.mission['4'].speedometerValue;

                        if(counter == 10){
                            counter = 0;
                            N = 160;
                        } else {
                            N = Math.floor(Math.random() * 161);
                        }
                    }
                }, 40);
            },
            stopSpeed: function(){
                clearInterval(game.mission['4'].timer);
                game.mission['4'].timer = null;
            },
            showLostLivesPopup(){
                customAlert('#lost-lives-popup');

                if(gameData.globalScore < 10){
                    $('.game-speed__10-points-btn').prop('disabled', true);
                } else {
                    $('.game-speed__10-points-btn').prop('disabled', false);
                }
            },
            showFriendRequestPopup(){
                customAlert('#friend-request-popup');

                if(gameData.globalScore < 10){
                    $('.game-speed__10-points-btn').prop('disabled', true);
                } else {
                    $('.game-speed__10-points-btn').prop('disabled', false);
                }
            },
            finish: function(){
                game.getCurrentMission();
            }
        },
        5: {
            start: function(){
                if(gameData.mission['5'].order){
                    $('.mission-5').attr('data-status', 'code');
                }

                $('.mission-5__promo').text(gameData.mission['5'].promo);

                $('.mission-5__order-btn').on('click', function(){
                    gameData.mission['5'].order = true;

                    game.setData();

                    $('.mission-5').attr('data-status', 'code');
                });

                $('.mission-5__next-btn').on('click', function(){
                    $('.mission-5').attr('data-status', 'code');
                });

                $('.mission-5__prev-btn').on('click', function(){
                    $('.mission-5').attr('data-status', 'start');
                });

                $('.mission-5__form').validate({
                    submitHandler: function(form) {
                        $('html').addClass('is-waiting');

                        $.ajax({
                            type: "POST",
                            url: $(form).attr('action'),
                            data: $(form).serialize()
                        }).done(function(data) {
                            $('html').removeClass('is-waiting');

                            if(data.error){
                                customAlert({
                                    icon: 'error',
                                    text: 'Неправильный код',
                                    button: true,
                                    buttonText: 'ДАЛЕЕ',
                                    buttonClass: 'popup-window__remove-btn',
                                });
                            } else {
                                game.mission['5'].finish();
                            }
                        });
                    }
                });
            },
            finish: function(){
                gameData.mission['5'].score += 20;
                gameData.globalScore += 20;
                $('.game-score__points').text(gameData.globalScore);

                gameData.finish = true;

                $('html').addClass('is-waiting');
                
                /* Отправляем игровую информацию на сервер */
                console.log(gameData);

                $.ajax({
                    type: "POST",
                    url: "/",
                    data: gameData
                }).done(function(data) {
                    $('html').removeClass('is-waiting');
                    
                    game.finish();
                });
            }
        }
    }
}