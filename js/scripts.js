// Cookies.remove('cookieAccept'); // <- удалить строку
var cookieAccept = Cookies.get('cookieAccept');




var preloadImagesList = new Array();
preloadImages(
    // 'images/input-active.svg'
)




$(document).ready(function() {
    /* Окно лоадера */
    loaderWindow();

    


    /* Слайдер призов */
    prizesSlider();
    
    $(document).on('mouseenter', '.prize', function(){
        if(isDesktop()){
            var $prize = $(this);

            $prize.addClass('is-hover');

            var prizeRotateInterval = setInterval(function(){
                if($prize.hasClass('is-hover')){
                    var $active = $prize.find('.prize__image img.is-active');
                    var $next = $active.next('img');

                    if(!$next.length){
                        clearInterval(prizeRotateInterval);
                    } else {
                        $active.removeClass('is-active');
                        $next.addClass('is-active');
                    }
                } else {
                    clearInterval(prizeRotateInterval);
                }
            }, 75);
        }
    });

    $(document).on('mouseleave', '.prize', function(){
        if(isDesktop()){
            var $prize = $(this);

            $prize.removeClass('is-hover');

            var prizeRotateInterval = setInterval(function(){
                if(!$prize.hasClass('is-hover')){
                    var $active = $prize.find('.prize__image img.is-active');
                    var $next = $active.prev('img');

                    if(!$next.length){
                        clearInterval(prizeRotateInterval);
                    } else {
                        $active.removeClass('is-active');
                        $next.addClass('is-active');
                    }
                } else {
                    clearInterval(prizeRotateInterval);
                }
            }, 75);
        }
    });




    /* Таблица рейтинг */
    var $ratingTableHeader = $('.rating-table__header');

    if($ratingTableHeader.length){
        observer.observe($ratingTableHeader[0]);
    }

    $('.rating-user-btn').on('click', function(){
        var $userRow = $('.rating-table__row._user');

        if($userRow.length){
            $('html, body').animate({
                scrollTop: $userRow.offset().top + $userRow.outerHeight() / 2 - $(window).height() / 2
            }, 500, function(){
                $userRow.addClass('is-highlight');
            });
        }
    });

    $(document).on('mouseenter', '.rating-table__row', function(){
        $('.rating-table__row').removeClass('is-highlight');
    });

    $(document).on('click', '.rating-table__body .rating-table__cell_points', function(){
        $(this).closest('.rating-table__row').toggleClass('is-open');
    });




    /* Меню */
    $('.burger-btn').on('click', function(){
        noscrollStart();
        $('html').addClass('is-menu-open').trigger('menu-open');
    });

    $('.menu-window__back').on('click', function(){
        $('html').removeClass('is-menu-open').trigger('menu-close');
        noscrollFinish();
    });




    /* Попап */
    $(document).on('click', '[data-popup]', function(e){
        var popupData = $(this).data('popup');

        customAlert(popupData);
    });

    $(document).on('click', '.popup-window__remove-btn, .popup__close-btn', function(e){
        var $popup = $(this).closest($('.popup-window'));
        
        $popup.removeClass('is-open');
        
        if($popup.hasClass('_dynamic')){
            setTimeout(function(){
                $popup.remove();
            }, 250);
        }
        
        if(!$('html').hasClass('is-login-open') && !$('html').hasClass('is-menu-open')){
            noscrollFinish();
        }
    });

    $(document).on('click', '.popup-window._closable .popup-window__content', function(e){
        if(!$(e.target).closest('.popup').length){
            $(this).find('.popup__close-btn').trigger('click');
        }
    });




    /* Окно куки */
    $('.cookie-alert__btn').on('click', function(){
        Cookies.set('cookieAccept', 'true');
        $('html').removeClass('is-cookie-alert-open');
    });




    /* Скопирование промокода */
    $(document).on('click', '.promo-copy', function(e){
        var $this = $(this);

        var tempElem = document.createElement('textarea');
        tempElem.value = $this.find('.promo-copy__text').text();
        document.body.appendChild(tempElem);
        tempElem.select();
        document.execCommand('copy');
        document.body.removeChild(tempElem);
        
        $this.addClass('is-copied');
        setTimeout(function(){
            $this.removeClass('is-copied');
        }, 2000);
    });




    /* Старт */
    $('.main__start-btn').on('click', function(){
        noscrollStart();
        $('html').addClass('is-login-open');
    });

    $('.login-window__close').on('click', function(){
        $('html').removeClass('is-login-open');
        noscrollFinish();
    });

    $('.login__back-btn').on('click', function(){
        $('.login__section_sms').hide();
        $('.login__section_reg').fadeIn();
    });

    $('.login__sms-control').keypress(function(e) {
        if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });




    /* Скролл к блоку */
    $('.js-target-link').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: $($(this).attr('href')).offset().top}, 700);
    });




    /* Старт Таймер */
    if($('.start-timer').length){
        var dateStart = Math.round(new Date($('.start-timer').data('start')).getTime() / 1000);

        var startTimer = setInterval(function(){
            var dateNow = Math.round(new Date().getTime() / 1000),
            delta = dateStart - dateNow,
            days, hours, minutes, seconds;

            if(delta > 0){
                days = Math.floor(delta / 86400);
                delta -= days * 86400;

                hours = Math.floor(delta / 3600) % 24;
                delta -= hours * 3600;

                minutes = Math.floor(delta / 60) % 60;
                delta -= minutes * 60;

                seconds = delta % 60;

                hours = ('0' + hours).slice(-2);
                minutes = ('0' + minutes).slice(-2);
                seconds = ('0' + seconds).slice(-2);
            } else {
                days = '0';
                hours = '00';
                minutes = '00';
                seconds = '00';

                clearInterval(startTimer);
            }
            
            $('.start-timer__days').text(days);
            $('.start-timer__time').text(hours + ':' + minutes + ':' + seconds);
        }, 1000);
    }




    /* Видео */
    $('.main__intro-video').on('click', function(e){
        e.preventDefault();

        noscrollStart();
        
        $('#intro-window').addClass('is-open');
        $('#intro-window video')[0].play();
    });

    $('.video-window__close').on('click', function(e){
        $(this).closest('.video-window').removeClass('is-open').find('video')[0].pause();

        noscrollFinish();
    });
});

$(window).on('resize', function(){
    /* Слайдер призов */
    prizesSlider();
});