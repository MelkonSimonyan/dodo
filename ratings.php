<?php $finish = false; ?>
<?php require 'blocks/header.php'; ?>

<div class="page">
	<div class="page__bg" style="background-image: url('images/main-start-bg.jpg');"></div>
	<div class="page__wrapper">
		<button type="button" class="page__burger burger-btn">
			<span class="burger-btn__lines">
				<span></span>
				<span></span>
				<span></span>
			</span>
			<span class="burger-btn__text">Меню</span>
		</button>

		<div class="page__logout">
			<a href="index.php" class="logout-btn">Выйти из игры</a>
		</div>

		<div class="container container_page">
			<div class="page__inner">
				<h1 class="page__title _lg">Рейтинг игроков</h1>

				<div class="rating-table-wrapper">
					<div class="rating-table">
						<div class="rating-table__header">
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">Место</div>
									<div class="rating-table__cell rating-table__cell_name">Игрок</div>
									<div class="rating-table__cell rating-table__cell_points">Итого в финале</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">1 Ур.</div>
									<div class="rating-table__cell rating-table__cell_level">2 Ур.</div>
									<div class="rating-table__cell rating-table__cell_level">3 Ур.</div>
									<div class="rating-table__cell rating-table__cell_level">4 Ур.</div>
									<div class="rating-table__cell rating-table__cell_level">5 Ур.</div>
									<div class="rating-table__cell rating-table__cell_prize">Приз</div>
								</div>
							</div>
						</div>
						<div class="rating-table__body">
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">1</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>Xbox Series X</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">2</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>Видеопроектор</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">3</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>Видеопроектор</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">4</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>Видеопроектор</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">5</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>VR-очки</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">6</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>VR-очки</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">7</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>VR-очки</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">8</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>VR-очки</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">9</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>VR-очки</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
						</div>

						<div class="rating-table__body">
							<div class="rating-table__group-title">Остальные герои</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">10</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>-</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">11</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>-</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">12</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>-</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">13</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>-</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">14</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>-</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">15</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>-</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">16</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>-</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">17</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>-</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">18</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>-</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
							<div class="rating-table__row _user">
								<div class="rating-table__main-group">
									<div class="rating-table__cell rating-table__cell_place">240</div>
									<div class="rating-table__cell rating-table__cell_name">Никнейм</div>
									<div class="rating-table__cell rating-table__cell_points">286</div>
								</div>
								<div class="rating-table__levels-group">
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_level">хх</div>
									<div class="rating-table__cell rating-table__cell_prize">
										<span>Приз:</span>
										<span>-</span>
									</div>
								</div>
								<div class="results-table__overlay"><span></span></div>
							</div>
						</div>
					</div>

					<div class="rating-table-pagination">
						<div class="pagination">
				            <a href="#" class="pagination__item pagination__first is-disabled">&laquo;</a>
				            <a href="#" class="pagination__item pagination__prev is-disabled">&lsaquo;</a>
				            <a href="#" class="pagination__item is-active">1</a>
				            <a href="#" class="pagination__item">2</a>
				            <a href="#" class="pagination__item">3</a>
				            <span class="pagination__item">...</span>
				            <a href="#" class="pagination__item">25</a>
				            <a href="#" class="pagination__item pagination__next">&rsaquo;</a>
				            <a href="#" class="pagination__item pagination__last">&raquo;</a>
				        </div>
					</div>

					<div class="rating-user-btn-wrapper">
						<div class="container container_page">
							<button type="button" class="rating-user-btn btn">
								<span>
									<span>Найти меня</span>
								</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require 'blocks/menu-window.php'; ?>

<?php require 'blocks/footer.php'; ?>