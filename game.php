<?php $finish = false; ?>
<?php $mission = isset($_GET['mission']) ? $_GET['mission'] : 1; ?>
<?php require 'blocks/header.php'; ?>

<script>
	var gameData = {
	    currentMission: <?= $mission; ?>,
	    finish: false,
	    globalScore: 0,
	    userName: "PrincessBANGBANG",
	    mission: {
	        1: {
	            score: 0
	        },
	        2: {
	            currentQuestion: 1,
	            friendRequest: false,
	            lives: 3,
	            score: 0
	        },
	        3: {
	        	promo: "",
	            score: 0
	        },
	        4: {
	        	friendRequest: false,
	            lives: 3,
	            score: 0
	        },
	        5: {
	        	order: false,
	        	promo: "",
	            score: 0
	        }
	    }
	};
</script>

<div class="game">
	<div class="game-interface">
		<div class="game-interface__top"></div>
		<div class="game-interface__bottom"></div>
		<div class="game-interface__left"></div>
		<div class="game-interface__right"></div>
	</div>
	
	<div class="game-copyright">©2021 20th Century Studios</div>
	
	<div class="game-score">
		<div class="game-score__title">баллы</div>
		<div class="game-score__points">0</div>
		<div class="game-score__username">PrincessBANGBANG</div>
	</div>
	
	<button type="button" class="game-burger burger-btn">
		<span class="burger-btn__lines">
			<span></span>
			<span></span>
			<span></span>
		</span>
		<span class="burger-btn__text">Меню</span>
	</button>
	
	<div class="game-missions"></div>
</div>

<script src="js/game.js?<?= time(); ?>"></script>

<?php require 'blocks/menu-window.php'; ?>

<?php require 'blocks/footer.php'; ?>