<div class="login-window">
    <button type="button" class="login-window__close close-btn"></button>

    <div class="login-window__inner scrollbar">
        <div class="login-window__copyright">©2021 20th Century Studios</div>
        <div class="login-window__content">
            <div class="login__section login__section_reg">
                <div class="login__section-title">Регистрация</div>
                
                <div class="login__reg">
                    <form class="login__reg-form">
                        <div class="form-control-wrapper">
                            <input type="text" class="login__name-control form-control" autocomplete="off" placeholder="Никнейм" name="login" required>
                            <div class="form-control-bg"></div>
                        </div>

                        <div class="form-control-wrapper">
                            <input type="text" class="login__phone-control form-control" autocomplete="off" placeholder="Телефон" name="phone" required inputmode="numeric" pattern="[0-9]*">
                            <div class="form-control-bg"></div>
                        </div>
                        
                        <div class="form-check">
                            <label class="form-check__label">
                                <input type="checkbox" class="form-check__input" name="aggreement" required checked>
                                <span class="form-check__text">Соглашаюсь с <a href="#">условиями сбора и обработки персональных данных,</a> а также <a href="#">правилами проведения акции</a></span>
                            </label>
                        </div>

                        <div class="form-btn-group">
                            <button type="submit" class="login__reg-btn btn">
                                <span>
                                    <span>ПРОДОЛЖИТЬ</span>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="login__section login__section_sms">
                <button type="button" class="login__back-btn"></button>
                
                <div class="login__section-title">Код из СМС</div>
                
                <div class="login__sms">
                    <form class="login__sms-form">
                        <div class="login__sms-title">Мы отправили СМС с кодом на номер <span class="login__sms-title-phone"></span></div>
                        
                        <div class="login__sms-control-row form-control-wrapper">
                            <input type="text" class="login__sms-control form-control" name="code" required inputmode="numeric" pattern="[0-9]*" autocomplete="one-time-code">
                            <div class="form-control-bg"></div>
                        </div>
                        
                        <div class="login__sms-timer-row">
                            Запросить код повторно можно через 
                            <span class="login__sms-timer-value">60</span>
                            секунды
                        </div>

                        <div class="login__sms-repeat-btn-row">
                            <a href="#" class="login__sms-repeat-btn">Отправить код повторно</a>
                        </div>
                            
                        <div class="login__sms-issue-btn-row">
                            <a href="#" class="login__sms-issue-btn">Не приходит СМС</a>
                        </div>

                        <div class="login__sms-start-btn-row">
                            <button type="submit" class="login__sms-start-btn btn">
                                <span>
                                    <span>СТАРТ</span>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>