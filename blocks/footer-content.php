<div class="footer">
    <div class="container">
        <div class="footer__top-wrapper">
            <ul class="footer__nav">
                <li class="footer__nav-item">
                    <a href="index.php" class="footer__nav-link">ГЛАВНАЯ</a>
                </li>
                <li class="footer__nav-item">
                    <a href="ratings.php" class="footer__nav-link">РЕЙТИНГ</a>
                </li>
                <li class="footer__nav-item">
                    <a href="rules.php" class="footer__nav-link">ПРАВИЛА</a>
                </li>
            </ul>

            <div class="footer__social">
                <a href="https://www.facebook.com/dodopizza" class="footer__social-btn" target="_blank">
                    <img src="images/footer-social-facebook.svg" alt="">
                </a>
                <a href="https://vk.com/dodo" class="footer__social-btn" target="_blank">
                    <img src="images/footer-social-vk.svg" alt="">
                </a>
                <a href="http://instagram.com/dodopizza" class="footer__social-btn" target="_blank">
                    <img src="images/footer-social-instagram.svg" alt="">
                </a>
                <a href="https://www.youtube.com/dodomovie" class="footer__social-btn" target="_blank">
                    <img src="images/footer-social-youtube.svg" alt="">
                </a>
            </div>
            
            <div class="footer__phone">
                <a href="tel:88003330060" class="footer__phone-num">8 800 333-00-60</a>
            </div>
        </div>

        <div class="footer__bottom-wrapper">
            <ul class="footer__second-nav">
                <li class="footer__second-nav-item">
                    <a href="info.php" class="footer__second-nav-link">Информация об организаторе</a>
                </li>
                <li class="footer__second-nav-item">
                    <a href="terms.php" class="footer__second-nav-link">Пользовательское соглашение</a>
                </li>
                <li class="footer__second-nav-item">
                    <a href="data-policy.php" class="footer__second-nav-link">Политика обработки персональных данных</a>
                </li>
            </ul>
            
            <div class="footer__logo">
                <img src="images/footer-logo.svg" alt="">
            </div>

            <div class="footer__copyright">© 2021</div>
        </div>
    </div>
</div>