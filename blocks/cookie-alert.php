<div class="cookie-alert">
    <div class="cookie-alert__content">Мы используем куки, чтобы пользоваться сайтом было удобно</div>
    <div class="cookie-alert__footer">
        <button type="button" class="cookie-alert__btn btn btn_static">Отлично</button>
    </div>
</div>