<!DOCTYPE HTML>
<html lang="ru" class="scrollbar is-loading <?= $finish ? "is-finish" : "" ?>">
<head>
    <meta charset="utf-8" />
    <title>Главный герой | Игра от Додо Пиццы и Disney</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />

    <link rel="preload" href="fonts/DodoRounded-Regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/DodoRounded-Medium.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/DodoRounded-Bold.woff2" as="font" type="font/woff2" crossorigin>

    <link href="css/style.css?<?= time(); ?>" rel="stylesheet" />
    <link href="css/tablet.css?<?= time(); ?>" rel="stylesheet" />
    <link href="css/mobile.css?<?= time(); ?>" rel="stylesheet" />

    <script>
        var vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', vh + 'px');

        window.addEventListener('resize', function(){
            var vh = window.innerHeight * 0.01;
            document.documentElement.style.setProperty('--vh', vh + 'px');
        });
    </script>
    <script src="lib/jquery-3.6.0/jquery-3.6.0.min.js"></script>
    <script src="lib/js-cookie-2.2.1/js.cookie.js"></script>
</head>

<body>