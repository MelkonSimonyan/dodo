    <?php require 'blocks/cookie-alert.php'; ?>

    <div class="waiting-window"></div>

    <link href="lib/slick-1.9.0/slick.min.css" rel="stylesheet" />
    
    <script src="lib/jquery.validation-1.19.1/jquery.validate.min.js"></script>
    <script src="lib/slick-1.9.0/slick.min.js"></script>
    <script src="lib/jquery.inputmask-4.0.1-37/jquery.inputmask.bundle.min.js"></script>
    
    <script src="js/functions.js?<?= time(); ?>"></script>
    <script src="js/scripts.js?<?= time(); ?>"></script>
    <script src="js/add.js?<?= time(); ?>"></script>
</body>
</html>