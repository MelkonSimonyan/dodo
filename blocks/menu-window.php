<div class="menu-window">
    <button type="button" class="menu-window__back back-btn"></button>

    <div class="menu-window__logout">
        <a href="index.php" class="logout-btn">Выйти из игры</a>
    </div>

    <div class="menu-window__inner scrollbar">
        <div class="menu-window__wrapper">
            <div class="menu-window__content">
                <ul class="menu-window__nav">
                    <li class="menu-window__nav-item">
                        <a href="index.php" class="menu-window__nav-link">
                            <span class="menu-window__nav-bg"><span><span></span></span></span>
                            <span class="menu-window__nav-text">Главная</span>
                        </a>
                    </li>
                    <li class="menu-window__nav-item">
                        <a href="ratings.php" class="menu-window__nav-link">
                            <span class="menu-window__nav-bg"><span><span></span></span></span>
                            <span class="menu-window__nav-text">Рейтинг</span>
                        </a>
                    </li>
                    <li class="menu-window__nav-item">
                        <a href="#" class="menu-window__nav-link">
                            <span class="menu-window__nav-bg"><span><span></span></span></span>
                            <span class="menu-window__nav-text">Правила</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="menu-window__footer">
                <div class="menu-window__user">
                    <div class="user">
                        <div class="user__name">PizzaShow</div>
                        <div class="user__data">
                            <div class="user__data-item">
                                <div class="user__data-title">Место</div>
                                <div class="user__data-value">59</div>
                            </div>

                            <div class="user__data-item">
                                <div class="user__data-title">Баллы</div>
                                <div class="user__data-value">86</div>
                            </div>

                            <div class="user__data-item user__data-item_promocode">
                                <div class="user__data-title">Промокод</div>
                                <div class="user__data-value">
                                    <div class="promo-copy">
                                        <div class="promo-copy__text">DODO2021PIZZA</div>
                                        <div class="promo-copy__tooltip">Текст скопирован!</div>
                                    </div>
                                </div>
                                <button type="button" class="user__data-question-mark question-mark-btn" data-popup='{"text": "<p>Акция действует в&nbsp;ресторане, на&nbsp;доставку и&nbsp;самовывоз до&nbsp;5&nbsp;сентября 2021&nbsp;года, один&nbsp;раз.</p><p>Акция не&nbsp;действует с&nbsp;комбо и&nbsp;на&nbsp;дополнительные ингредиенты. Скидки по&nbsp;промокодам не&nbsp;суммируются. Предложение действует, пока есть&nbsp;товар в&nbsp;наличии.</p>", "theme": "blue", "size": "lg", "closeBtn": true}'></button>
                            </div>
                        </div>
                    </div>
                </div>

                <a href="#" class="menu-window__order-btn btn">
                    <span>
                        <span>Заказать пиццу</span>
                    </span>
                </a>
                
                <div class="menu-window__social">
                    <div class="social">
                        <a href="https://www.facebook.com/dodopizza" class="social__btn" target="_blank">
                            <span class="social__btn-wrapper">
                                <img src="images/social-fb.svg" alt="">
                                <img src="images/social-fb-active.svg" alt="">
                            </span>
                        </a>
                        <a href="https://vk.com/dodo" class="social__btn" target="_blank">
                            <span class="social__btn-wrapper">
                                <img src="images/social-vk.svg" alt="">
                                <img src="images/social-vk-active.svg" alt="">
                            </span>
                        </a>
                        <a href="http://instagram.com/dodopizza" class="social__btn" target="_blank">
                            <span class="social__btn-wrapper">
                                <img src="images/social-ig.svg" alt="">
                                <img src="images/social-ig-active.svg" alt="">
                            </span>
                        </a>
                        <a href="https://www.youtube.com/dodomovie" class="social__btn" target="_blank">
                            <span class="social__btn-wrapper">
                                <img src="images/social-yt.svg" alt="">
                                <img src="images/social-yt-active.svg" alt="">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>