<?php $finish = false; ?>
<?php require 'blocks/header.php'; ?>

<div class="page">
	<div class="page__bg" style="background-image: url('images/main-start-bg.jpg');"></div>
	<div class="page__wrapper">
		<button type="button" class="page__burger burger-btn">
			<span class="burger-btn__lines">
				<span></span>
				<span></span>
				<span></span>
			</span>
			<span class="burger-btn__text">Меню</span>
		</button>

		<div class="page__logout">
			<a href="index.php" class="logout-btn">Выйти из игры</a>
		</div>

		<div class="container container_page">
			<div class="page__inner">
				<h1 class="page__title">Информация об организаторе: «Додо Франчайзинг»</h1>

				<div class="text">
					<ul>
						<li>Полное наименование организации: Общество с ограниченной ответственностью «Додо Франчайзинг»</li>
						<li>Юридический адрес: 167001, Республика Коми, г.Сыктывкар, Октябрьский проспект, д. 16</li>
						<li>ИНН: 1101140415</li>
						<li>КПП: 110101001</li>
						<li>Руководитель организации: Петелин Андрей Алексеевич, действующий на основании Устава</li>
						<li>Контактный телефон: <a href="tel:88003330060" style="white-space: nowrap;">8 800 333-00-60</a></li>
						<li>Электронная почта: <a href="mailto:feedback@dodopizza.com">feedback@dodopizza.com</a></li>
					</ul>
					<br>
					<br>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require 'blocks/footer-content.php'; ?>

<?php require 'blocks/menu-window.php'; ?>

<?php require 'blocks/footer.php'; ?>