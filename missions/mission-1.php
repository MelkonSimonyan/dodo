<div class="mission mission-1" data-status="start">
	<div class="mission__bg" style="background-image: url('images/mission-1.jpg');"></div>
	
	<div class="mission__start">
		<div class="mission__start-content">
			<div class="mission__num">МИССИЯ 1/5</div>
			<div class="mission__title">
				<span class="mission__title-bg"><span><span></span></span></span>
				<span class="mission__title-text">УТРО ВО FREE CITY</span>
			</div>
			<div class="mission__text">Помоги парню зарядиться энергией. Найди как можно больше Додо‑стаканчиков с&nbsp;кофе, спрятанных&nbsp;в&nbsp;городе.</div>
			<button type="button" class="mission-1__start-btn mission__start-btn btn">
				<span>
					<span>СТАРТ</span>
				</span>
			</button>
			<div class="mission-1__start-coffee">
				<img src="images/coffee/02.png" style="width: 38.8rem; height: 49.7rem;" alt="" class="is-active">
				<img src="images/coffee/03.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/04.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/05.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/06.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/07.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/08.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/09.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/10.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/11.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/12.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/13.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/14.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/15.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/16.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/17.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/18.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/19.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/20.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/21.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/22.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/23.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/24.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/25.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/26.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/27.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/28.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/29.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/30.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/31.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/32.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/33.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/34.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/35.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/36.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/37.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/38.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/39.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/40.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/41.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/42.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/43.png" style="width: 38.8rem; height: 49.7rem;" alt="">
				<img src="images/coffee/44.png" style="width: 38.8rem; height: 49.7rem;" alt="">
			</div>
		</div>

		<div class="mission__icon"></div>
	</div>

	<div class="game-map">
		<div class="game-map__bg" style="background-image: url('images/map.jpg');"></div>
		
		<div class="game-map__header">
			<div class="game-map__title">ВЫБЕРИТЕ ЛОКАЦИЮ</div>
			<div class="game-map__subtitle">Или подкрепитесь нашей вкусной и свежей пиццей :)</div>
		</div>
		
		<div class="game-map__markers">
			<button type="button" class="game-map__marker game-map__marker_location game-map__marker_location-1" data-location="location-1">
				<div class="game-map__marker-content">
					<div class="game-map__marker-icon">
						<span style="background-image: url('images/location-marker-bg-1.png');"></span>
					</div>
					<div class="game-map__marker-title">
						<span>Работа Парня</span>
					</div>
				</div>
			</button>

			<button type="button" class="game-map__marker game-map__marker_location game-map__marker_location-2" data-location="location-2">
				<div class="game-map__marker-content">
					<div class="game-map__marker-icon">
						<span style="background-image: url('images/location-marker-bg-2.png');"></span>
					</div>
					<div class="game-map__marker-title">
						<span>Пиццерия</span>
					</div>
				</div>
			</button>

			<a href="https://dodopizza.ru/" class="game-map__marker game-map__marker_dodo" target="_blank">
				<div class="game-map__marker-content">
					<div class="game-map__marker-icon">
						<span style="background-image: url('images/location-marker-bg-dodo.png');"></span>
					</div>
					<div class="game-map__marker-title">
						<span>Проголодался? Зайди</span>
						<span>в пиццерию Додо</span>
					</div>
				</div>
			</a>
		</div>
	</div>

	<div class="game-location game-location_1" data-location="location-1">
		<div class="game-location__bg" style="background-image: url('images/mission-location-1.jpg');"></div>
		
		<div class="game-location__timer game-timer">0:30</div>
		
		<div class="game-location__scale">
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
		</div>
		
		<div class="game-location__markers">
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
		</div>
	</div>

	<div class="game-location game-location_2" data-location="location-2">
		<div class="game-location__bg" style="background-image: url('images/mission-location-2.jpg');"></div>
		
		<div class="game-location__timer game-timer">0:30</div>
		
		<div class="game-location__scale">
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
			<div class="game-location__scale-item"></div>
		</div>
		
		<div class="game-location__markers">
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
			<button type="button" class="game-location__marker">
				<span class="game-location__marker-icon"></span>
			</button>
		</div>
	</div>

	<div class="game-help game-help_score">
		<div class="game-help__content">
			<div class="game-help__text">Это твои баллы. Набери максимальное количество, чтобы стать Главным героем!</div>
			<button type="button" class="game-help__btn btn">
				<span>
					<span>ПОНЯТНО</span>
				</span>
			</button>
		</div>
		<div class="game-help__bg"></div>
	</div>

	<div class="game-help game-help_timer">
		<div class="game-help__content">
			<div class="game-help__text">На каждую локацию у тебя <span class="game-help_timer-value"></span> секунд. Найди как можно больше стаканчиков за&nbsp;это&nbsp;время. </div>
			<button type="button" class="game-help__btn btn">
				<span>
					<span>ПОНЯТНО</span>
				</span>
			</button>
		</div>
		<div class="game-help__bg"></div>
	</div>

	<div class="game-help game-help_scale">
		<div class="game-help__content">
			<div class="game-help__text">Это шкала с найденными стаканчиками: чем больше ты найдёшь — тем больше баллов заработаешь.</div>
			<button type="button" class="game-help__btn btn">
				<span>
					<span>ПОНЯТНО</span>
				</span>
			</button>
		</div>
		<div class="game-help__bg"></div>
	</div>

	<div class="game-help game-help_start">
		<div class="game-help__content">
			<div class="game-help__icons">
				<div class="d-md-none">
					<img src="images/cursor-active.svg" style="width: 7rem; height: 7rem;" alt="">
					<img src="images/cursor.svg" style="width: 7rem; height: 7rem;" alt="">
				</div>
				<div class="d-none d-md-block">
					<img src="images/tap.svg" style="width: 6.5rem; height: 6.3rem;" alt="">
				</div>
			</div>
			<div class="game-help__text">
				<div class="d-md-none">Найди Додо-стаканчики с кофе в городе: наведи стрелкой на любое место и если она покраснеет — стаканчик найден!</div>
				<div class="d-none d-md-block">Найди Додо-стаканчики с кофе в городе — нажимай пальчиком на экран</div>
			</div>
			<button type="button" class="game-help__btn btn">
				<span>
					<span>СТАРТ</span>
				</span>
			</button>
		</div>
		<div class="game-help__bg"></div>
	</div>
</div>