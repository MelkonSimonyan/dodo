<div class="mission mission-4" data-status="start">
	<div class="mission__bg" style="background-image: url('images/mission-4.jpg');"></div>
	
	<div class="mission__start">
		<div class="mission__start-content">
			<div class="mission__num">МИССИЯ 4/5</div>
			<div class="mission__title">
				<span class="mission__title-bg"><span><span></span></span></span>
				<span class="mission__title-text">СПАСИ Molotov Girl</span>
			</div>
			<div class="mission__text">Molotov girl в опасности — вырвись из&nbsp;амбара, набрав максимальную скорость на спидометре.</div>
			<button type="button" class="mission-4__start-btn mission__start-btn btn">
				<span>
					<span>СТАРТ</span>
				</span>
			</button>
		</div>

		<div class="mission__icon"></div>
	</div>

	<div class="game-speed">
		<div class="game-speed__lives game-lives">
			<div class="game-lives__text">х <span class="game-lives__count">3</span></div>
		</div>
		
		<div class="game-speed__content">
			<div class="game-speed__circle">
				<div class="game-speed__circle-light"></div>
				<div class="game-speed__circle-bg"></div>
				<div class="game-speed__circle-num"></div>
				<div class="game-speed__circle-line-wrapper">
					<svg class="game-speed__circle-line" viewbox="0 0 490 490" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					    <path d=" M 24 339 A 240 240 157 1 1 466 339" />
					</svg>
				</div>
				<div class="game-speed__circle-140"></div>
				<button type="button" class="game-speed__circle-btn"></button>
			</div>
		</div>
	</div>

	<div class="game-help game-help_speed">
		<div class="game-help__content">
			<div class="game-help__text">Нажми на кнопку «Газуй», когда<br> скорость будет свыше 140 км/ч</div>
			<button type="button" class="game-help__btn btn">
				<span>
					<span>СТАРТ</span>
				</span>
			</button>
		</div>
		<div class="game-help__bg"></div>
	</div>

	<div class="popup-window" id="lost-lives-popup">
	    <div class="popup-window__content scrollbar">
	        <div class="popup popup_size_lg">
	            <div class="popup__content">
	                <div class="popup__content-inner">
	                    <div class="popup__title">Ауч!</div>
	                    <div class="popup__text">Вы потеряли все жизни. Для возобновления игры у вас есть два выхода:</div>
	                    <div class="popup__footer">
	                        <div class="popup__footer-item">
	                            <button type="button" class="popup__link popup-window__remove-btn game-speed__friend-request-btn">Позвать друга</button>
	                        </div>
	                        <div class="popup__footer-item">
	                            <button type="button" class="popup__link popup-window__remove-btn game-speed__10-points-btn">Потратить 10 баллов</button>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="popup__dec"><span></span><span><span></span><span></span><span></span><span></span></span><span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span><span><span></span><span></span></span></div>
	        </div>
	    </div>
	</div>

	<div class="popup-window" id="friend-request-popup">
	    <div class="popup-window__content scrollbar">
	        <div class="popup popup_size_lg popup_theme_blue">
	            <div class="popup__content">
	                <div class="popup__content-inner">
	                    <div class="popup__text">Скопируй ссылку и отправь другу! Как только он подключится к игре, ты сможешь вернуться в игру</div>
	                    <div class="popup__promo-copy promo-copy">
							<div class="promo-copy__text">dodopizza.ru/moscow?utm_source=google</div>
							<div class="promo-copy__tooltip">Текст скопирован!</div>
	                    </div>
	                    <div class="friend-request-text">Или ты можешь потратить баллы и продолжить играть прямо сейчас!</div>
	                    <div class="popup__footer">
	                        <div class="popup__footer-item">
	                            <button type="button" class="popup__link game-speed__friend-submit-btn">Друг пришел?</button>
	                        </div>
	                        <div class="popup__footer-item">
	                            <button type="button" class="popup__link popup-window__remove-btn game-speed__10-points-btn">Потратить 10 баллов</button>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="popup__dec"><span></span><span><span></span><span></span><span></span><span></span></span><span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span><span><span></span><span></span></span></div>
	        </div>
	    </div>
	</div>

	<div class="mission-4-victory-video-window video-window">
	    <video preload playsinline webkit-playsinline>
	        <!-- <source src="video/intro.webm" type="video/webm" /> -->
	        <source src="video/mission-4-victory.mp4" type="video/mp4" />
	    </video>
	</div>

	<div class="mission-4-losing-video-window video-window">
	    <video preload playsinline webkit-playsinline>
	        <!-- <source src="video/intro.webm" type="video/webm" /> -->
	        <source src="video/mission-4-losing.mp4" type="video/mp4" />
	    </video>
	</div>
</div>