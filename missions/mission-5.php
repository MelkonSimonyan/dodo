<div class="mission mission-5" data-status="start">
	<div class="mission__bg" style="background-image: url('images/mission-5.jpg');"></div>
	
	<div class="mission__start">
		<div class="mission__start-content">
			<div class="mission__num">МИССИЯ 5/5</div>
			<div class="mission__title">
				<span class="mission__title-bg"><span><span></span></span></span>
				<span class="mission__title-text">ФИНАЛЬНАЯ БИТВА</span>
			</div>
			<div class="mission__text">Настало время финальной битвы: жители города против Tsunami Developers.</div>
			<div class="mission__final-steps">
				<div class="mission__final-steps-item">Подкрепи силы пиццей! Промокод дает 15% скидку на любую пиццу 35 см</div>
				<div class="mission__final-steps-item">Вместе с пиццей придет кодовое слово для победы над Tsunami Developers</div>
				<div class="mission__final-steps-item">Введи кодовое слово на сайте, освободи город и стань героем!</div>
			</div>
			<div class="mission-5__promo-section">
				<div class="mission-5__promo-wrapper">
					<div class="mission__input-wrapper _promo">
						<div class="mission__input">
							<div class="mission__input-control-wrapper">
								<div class="promo-copy">
									<div class="promo-copy__text mission-5__promo"></div>
									<div class="promo-copy__tooltip">Текст скопирован!</div>
								</div>
							</div>
							<div class="mission__input-btn-wrapper">
								<a href="http://dodopizza.ru/" class="mission-5__order-btn mission__input-btn btn" target="_blank">
									<span>
										<span>ЗАКАЗАТЬ ПИЦЦУ</span>
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="mission-5__next-btn-wrapper">
						<button type="button" class="mission-5__next-btn">Далее</button>
					</div>
				</div>
				<div class="mission-5__form-wrapper">
					<form class="mission-5__form mission__input-wrapper _form">
						<div class="mission__input">
							<div class="mission__input-control-wrapper">
								<input type="text" class="mission__input-control" placeholder="кодовое слово" name="word" required>
							</div>
							<div class="mission__input-btn-wrapper">
								<button type="submit" class="mission__input-btn btn">
									<span>
										<span>СТАРТ</span>
									</span>
								</button>
							</div>
						</div>
					</form>
					<div class="mission-5__next-btn-wrapper">
						<button type="button" class="mission-5__prev-btn">Назад</button>
					</div>
				</div>
			</div>
		</div>

		<div class="mission__icon"></div>
	</div>
</div>