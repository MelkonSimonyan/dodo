<div class="mission mission-2" data-status="start">
	<div class="mission__bg" style="background-image: url('images/mission-2.jpg');"></div>
	
	<div class="mission__start">
		<div class="mission__start-content">
			<div class="mission__num">МИССИЯ 2/5</div>
			<div class="mission__title">
				<span class="mission__title-bg"><span><span></span></span></span>
				<span class="mission__title-text">ВЫЖИТЬ ВО FREE&nbsp;CITY</span>
			</div>
			<button type="button" class="mission-2__play-btn mission__play-btn"></button>
		</div>

		<div class="mission__icon"></div>
	</div>

	<div class="game-test">
		<div class="game-test__timer game-timer">0:15</div>
		
		<div class="game-test__lives game-lives">
			<div class="game-lives__text">х <span class="game-lives__count">3</span></div>
		</div>
		
		<div class="game-test__content">
			<div class="game-test__item" data-id="1">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/01.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">01 / 15</div>
							<div class="game-test__item-question">Солнце встало над Фрисити, значит вам и вашему персонажу пора на работу. Ваши действия?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Я не спешу, поставлю чайник греться</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Я же опаздываю, куплю кофе по дороге</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="2">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/02.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">02 / 15</div>
							<div class="game-test__item-question">Вы выбежали из дома. На дороге открытый люк. Что вы сделаете?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Перепрыгну, я ловкий.</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Осторожность — мое второе имя. Обойду.</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="3">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/03.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">03 / 15</div>
							<div class="game-test__item-question">Вы направляетесь дальше в сторону работы. Вдруг из-за угла выбегает мужчина в маске с мешком денег в руках. Как вы поступите?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Перебегу на противоположную сторону дороги.</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Поставлю подножку, помогу бравой полиции.</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="4">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/04.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">04 / 15</div>
							<div class="game-test__item-question">Вы подошли к кофейне одновременно с бабулей в шапочке. Кто войдет первым?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Открою дверь перед бабулей, пропущу вперед</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Я опаздываю, мне бы кофе, и на работу быстрее</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="5">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/05.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">05 / 15</div>
							<div class="game-test__item-question">Вы дошли до следующей кофейни, взяли кофе. На выходе понимаете — шнурки развязались.</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Встану на одно колено, поставлю рядом чашку.</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Завяжу в метро. Что такого может произойти?</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="6">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/06.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">06 / 15</div>
							<div class="game-test__item-question">Вы зашли в метро, но что это? Поезд скоро отправится — забегайте в вагон быстрее.</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Я лучше подожду следующий</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Я успею!</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="7">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/07.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">08 / 15</div>
							<div class="game-test__item-question">Поезд подъезжает. Хорошо, нет пересадок. Как будете подниматься?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Поехал на лифте</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Пошел пешком по эскалатору!</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="8">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/08.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">08 / 15</div>
							<div class="game-test__item-question">О, остановка. Подождете на остановке или чуть дальше от людей?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">На остановке, там тень</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">За остановкой, я туда</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="9">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/09.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">09 / 15</div>
							<div class="game-test__item-question">Вы ждете автобус и видите девушку, идущую в вашу сторону. Вы решитесь познакомиться?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Да, решусь</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Сейчас не время для новых знакомств</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="10">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/10.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">10 / 15</div>
							<div class="game-test__item-question">На автобусе вы неплохо прокатились, не правда ли? Куда пойдете дальше?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Уже почти у офиса. Захожу.</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Тут все воюют. Надо снять на телефон</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="11">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/11.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">11 / 15</div>
							<div class="game-test__item-question">Повеселились и хватит. На входе вы встречаете доброго охранника. Он вас приветствует.</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Прохожу мимо кивнув</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Останавливаюсь поболтать с охраной</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="12">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/12.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">12 / 15</div>
							<div class="game-test__item-question">О, опять подниматься. Лифт или лестница?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Поехал на лифте</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Пошел пешком по лестнице!</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="13">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/13.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">13 / 15</div>
							<div class="game-test__item-question">Вы проходите мимо кабинета подчиненных. Слышите смех. Загляните?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Да</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Нет</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="14">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/14.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">14 / 15</div>
							<div class="game-test__item-question">Пришло время позавтракать. Что вы выберите?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Додо Пицца</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Додо Пицца</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="game-test__item" data-id="15">
				<div class="game-test__item-inner">
					<div class="game-test__item-image">
						<img src="images/test/15.png" alt="">
					</div>
					<div class="game-test__item-content">
						<div class="game-test__item-header">
							<div class="game-test__item-num">15 / 15</div>
							<div class="game-test__item-question">А что заказать?</div>
						</div>
						<div class="game-test__item-answers">
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Пиццу</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
							<div class="game-test__item-answer">
								<div class="game-test__item-answer-inner">
									<div class="game-test__item-answer-content">Додстер</div>
									<div class="game-test__item-answer-overlay"><span></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="game-help game-help_lives">
		<div class="game-help__content">
			<div class="game-help__text">Это ваши жизни. У вас есть всего три попытки на выживание.</div>
			<button type="button" class="game-help__btn btn">
				<span>
					<span>ДАЛЕЕ</span>
				</span>
			</button>
		</div>
		<div class="game-help__bg"></div>
	</div>

	<div class="game-help game-help_timer">
		<div class="game-help__content">
			<div class="game-help__text">Это таймер. На каждый вопрос дается 15 секунд. Желаем вам выжить!</div>
			<button type="button" class="game-help__btn btn">
				<span>
					<span>СТАРТ</span>
				</span>
			</button>
		</div>
		<div class="game-help__bg"></div>
	</div>

	<div class="popup-window" id="lost-lives-popup">
	    <div class="popup-window__content scrollbar">
	        <div class="popup popup_size_lg">
	            <div class="popup__content">
	                <div class="popup__content-inner">
	                    <div class="popup__title">Ауч!</div>
	                    <div class="popup__text">Вы потеряли все жизни. Для возобновления игры у вас есть два выхода:</div>
	                    <div class="popup__footer">
	                        <div class="popup__footer-item">
	                            <button type="button" class="popup__link popup-window__remove-btn game-test__friend-request-btn">Позвать друга</button>
	                        </div>
	                        <div class="popup__footer-item">
	                            <button type="button" class="popup__link popup-window__remove-btn game-test__10-points-btn">Потратить 10 баллов</button>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="popup__dec"><span></span><span><span></span><span></span><span></span><span></span></span><span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span><span><span></span><span></span></span></div>
	        </div>
	    </div>
	</div>

	<div class="popup-window" id="friend-request-popup">
	    <div class="popup-window__content scrollbar">
	        <div class="popup popup_size_lg popup_theme_blue">
	            <div class="popup__content">
	                <div class="popup__content-inner">
	                    <div class="popup__text">Скопируй ссылку и отправь другу! Как только он подключится к игре, ты сможешь вернуться в игру</div>
	                    <div class="popup__promo-copy promo-copy">
							<div class="promo-copy__text">dodopizza.ru/moscow?utm_source=google</div>
							<div class="promo-copy__tooltip">Текст скопирован!</div>
	                    </div>
	                    <div class="friend-request-text">Или ты можешь потратить баллы и продолжить играть прямо сейчас!</div>
	                    <div class="popup__footer">
	                        <div class="popup__footer-item">
	                            <button type="button" class="popup__link game-test__friend-submit-btn">Друг пришел?</button>
	                        </div>
	                        <div class="popup__footer-item">
	                            <button type="button" class="popup__link popup-window__remove-btn game-test__10-points-btn">Потратить 10 баллов</button>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="popup__dec"><span></span><span><span></span><span></span><span></span><span></span></span><span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span><span><span></span><span></span></span></div>
	        </div>
	    </div>
	</div>

	<div class="mission-2-video-window video-window">
	    <video preload playsinline webkit-playsinline>
	        <!-- <source src="video/intro.webm" type="video/webm" /> -->
	        <source src="video/mission-2.mp4" type="video/mp4" />
	    </video>
	</div>

	<script>
		var test = {
			"1" : {
				"rightAnswer": "2",
				"successMessage": "Отлично, вы получаете первое очко!",
				"errorMessage": "Упс, в чайнике нет воды. Сгорели все ваши вещи."
			},
			"2" : {
				"rightAnswer": "1",
				"successMessage": "Это было опасно, вы приземлились на самый край. Но нам нравится ваш настрой.",
				"errorMessage": "Вы встали между дуэлянтами совсем не вовремя. Поговорим в следующей жизни."
			},
			"3" : {
				"rightAnswer": "2",
				"successMessage": "Вы — герой! Продолжайте путь на работу, пока полиция задерживает преступника.",
				"errorMessage": "Вы перебежали прямо на тот свет. Осторожнее."
			},
			"4" : {
				"rightAnswer": "1",
				"successMessage": "Бабушка благодарит, натягивает шапку на лицо и достает пистолет. Напоследок говорит: «Иди, милый, рядом еще кофейня.»",
				"errorMessage": "Вы не пропускаете бабулю, у кассы слышите щелчок дробовика и крик «Это ограбление». Выстрел, и ваш персонаж на полу"
			},
			"5" : {
				"rightAnswer": "1",
				"successMessage": "Ого, ракета пролетела мимо. Вы вовремя нагнулись!",
				"errorMessage": "Вы стоите в полный рост, поэтому случайная ракета из РПГ попадает в вас. Повезет в следующий раз!"
			},
			"6" : {
				"rightAnswer": "2",
				"successMessage": "Вы успели, но ценой чего? Правильно, бесплатно. Двигайтесь дальше.",
				"errorMessage": "Вы его не дождетесь. На станции работают снайперы"
			},
			"7" : {
				"rightAnswer": "2",
				"successMessage": "Ой, а там и правда нет народу. Выбегайте с ветерком.",
				"errorMessage": "Жаль вы не увидели, что трос у него был слабым."
			},
			"8" : {
				"rightAnswer": "2",
				"successMessage": "Под лавкой лежало 30 долларов. У вас очень удачный день.",
				"errorMessage": "Сегодня же День мин на остановках, вы чего?"
			},
			"9" : {
				"rightAnswer": "2",
				"successMessage": "Отлично! Нам нравится ваш настрой на работу.",
				"errorMessage": "Девушкой оказался 35-летний парень. В сети нельзя никому доверять на 100%."
			},
			"10" : {
				"rightAnswer": "2",
				"successMessage": "Вы получили 7000 реакций в Инстаграме. Потрясающе!",
				"errorMessage": "Рояль закрепили не очень хорошо. Вы познакомились с ним очень близко."
			},
			"11" : {
				"rightAnswer": "2",
				"successMessage": "Почему бы не поговорить с хорошим человеком!",
				"errorMessage": "Ну, куда по мытому!  Неверное движение, и вы на кафеле."
			},
			"12" : {
				"rightAnswer": "1",
				"successMessage": "Жаль вы не увидели, что трос у него был сильным. Это верное решение.",
				"errorMessage": "Ой, а вы не почувствовали легкого толчка? Лестница порушилась, она же из вафель."
			},
			"13" : {
				"rightAnswer": "2",
				"successMessage": "Пусть работают в своем темпе. Тем более, звук стрельбы не повод заходить в комнату.",
				"errorMessage": "Представляете, там у них дуэль! Вот парень увернулся от пули, а вы нет."
			},
			"14" : {
				"rightAnswer": "0",
				"successMessage": "Ура, пицца!",
				"errorMessage": "Ура, пицца!"
			},
			"15" : {
				"rightAnswer": "0",
				"successMessage": "Хороший выбор!",
				"errorMessage": "Прекрасно!"
			}
		};
	</script>
</div>