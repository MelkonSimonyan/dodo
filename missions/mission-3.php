<div class="mission mission-3" data-status="start">
	<div class="mission__bg" style="background-image: url('images/mission-3.jpg');"></div>
	
	<div class="mission__start">
		<div class="mission__start-content">
			<div class="mission__num">МИССИЯ 3/5</div>
			
			<div class="mission-3__start">
				<div class="mission__title">
					<span class="mission__title-bg"><span><span></span></span></span>
					<span class="mission__title-text">УЗНАЙ СЕКРЕТНЫЙ КОД</span>
				</div>
				<div class="mission__text">Программа уничтожения города запущена,  узнай секретный код безопасности —&nbsp;расшифруй послание во <a href="https://vk.com/dodo" target="_blank">ВКонтакте</a> Додо Пиццы.</div>
				<form class="mission-3__form mission__input-wrapper _form">
					<div class="mission__input">
						<div class="mission__input-control-wrapper">
							<input type="text" class="mission__input-control" placeholder="кодовое слово" name="word" required>
						</div>
						<div class="mission__input-btn-wrapper">
							<button type="submit" class="mission__input-btn btn">
								<span>
									<span>СТАРТ</span>
								</span>
							</button>
						</div>
					</div>
				</form>
			</div>

			<div class="mission-3__win">
				<div class="mission__title">
					<span class="mission__title-bg"><span><span></span></span></span>
					<span class="mission__title-text">Успешная деактивация</span>
				</div>
				<div class="mission__text">Молодец! Ты в двух шагах от победы!<br> Закажи пиццу по промокоду и зарядись энергией.</div>
				<button type="button" class="mission-3__finish-btn mission__start-btn btn">
					<span>
						<span>ДАЛЕЕ</span>
					</span>
				</button>
			</div>
		</div>

		<div class="mission__icon"></div>
	</div>

	<div class="mission-3__popup-wrapper">
        <div class="popup popup_size_lg popup_theme_blue">
            <div class="popup__content">
                <div class="popup__content-inner">
                    <div class="popup__text">Мясная пицца 25 см в подарок<br> при заказе от 799 ₽</div>
                    <div class="popup__promo-copy promo-copy">
						<div class="promo-copy__text mission-3__promo"></div>
						<div class="promo-copy__tooltip">Текст скопирован!</div>
                    </div>
                </div>
            </div>
            <div class="popup__dec"><span></span><span><span></span><span></span><span></span><span></span></span><span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span><span><span></span><span></span></span></div>
        </div>
	</div>
</div>