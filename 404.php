<?php $finish = false; ?>
<?php require 'blocks/header.php'; ?>

<div class="b-404">
	<div class="container">
		<div class="b-404__wrapper">
			<div class="main__logo">
				<a href="index.php"><img src="images/logo.svg" alt=""></a>
			</div>

			<div class="b-404__inner">
				<div class="b-404__header">
					<div class="b-404__header-title">
						<img src="images/404.svg" style="width: 68rem; height: 30.5rem;" alt="">
					</div>
					<div class="b-404__header-image">
						<img src="images/404-img.png" style="width: 67rem; height: 11.5rem;" alt="">
					</div>
				</div>
				<div class="b-404__content">
					<div class="b-404__title">Тут ты не<br> спасёшь мир! :(</div>
					<div class="b-404__text">Вернись на <a href="https://play.dodopizza.ru/">play.dodopizza.ru</a> и&nbsp;продолжай сражение!</div>
					<div class="b-404__btn-wrapper">
						<a href="index.php" class="b-404__btn btn">
							<span>
								<span>Вернуться</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require 'blocks/footer.php'; ?>