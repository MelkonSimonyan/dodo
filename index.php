<?php $timer = isset($_GET['timer']); ?>
<?php $finish = isset($_GET['finish']); ?>
<?php require 'blocks/header.php'; ?>

<div class="loader">
	<div class="loader__main">
		<div class="loader__inner">
			<div class="container">
				<div class="loader__content">
					<div class="loader__title">
						<?php if(!$finish){ ?>
							<span class="loader__title-t1">стань главным героем</span>
							<span class="loader__title-t2">в ДОДО-игре</span>
						<?php } else { ?>
							<span class="loader__title-t1">Ты&nbsp;спас Free&nbsp;City</span>
							<span class="loader__title-t2">и&nbsp;превратился в&nbsp;настоящего героя!</span>
						<?php } ?>
					</div>
					<div class="loader__text">
						<span class="is-active">
							<?php if(!$finish){ ?>
								Пройди все уровни игры
							<?php } else { ?>
								Ты прошел все уровни игры
							<?php } ?>
						</span>
						<span>
							<?php if(!$finish){ ?>
								Подкрепись пиццей
							<?php } else { ?>
								Ты подкрепился пиццей
							<?php } ?>
						</span>
						<span>
							<?php if(!$finish){ ?>
								Выиграй главный приз
							<?php } else { ?>
								Жди результатов!
							<?php } ?>
						</span>
					</div>
				</div>
			</div>
			<div class="loader__loader">
				<span></span>
			</div>
		</div>
		<div class="loader__image">
			<img src="images/loader-cover.jpg" alt="">
		</div>
	</div>
	
	<div class="loader__icon">
		<div class="loader__icon-c"></div>
		<div class="loader__icon-c"></div>
		<div class="loader__icon-c"></div>
		<div class="loader__icon-i"></div>
	</div>
	
	<div class="loader__footer">
		<div class="container">
			<div class="loader__steps">
				<div class="loader__steps-item">
					<span class="loader__steps-num">01</span>
					<span class="loader__steps-title">
						<?php if(!$finish){ ?>
							Пройди все уровни игры
						<?php } else { ?>
							Ты прошел все уровни игры
						<?php } ?>
					</span>
				</div>
				<div class="loader__steps-item">
					<span class="loader__steps-num">02</span>
					<span class="loader__steps-title">
						<?php if(!$finish){ ?>
							Подкрепись пиццей
						<?php } else { ?>
							Ты подкрепился пиццей
						<?php } ?>
					</span>
				</div>
				<div class="loader__steps-item">
					<span class="loader__steps-num">03</span>
					<span class="loader__steps-title">
						<?php if(!$finish){ ?>
							Выиграй главный приз
						<?php } else { ?>
							Жди результатов!
						<?php } ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="main-page">
	<?php if($finish){ ?>
		<div class="main-page__bg" style="background-image: url('images/main-finish-bg.jpg');"></div>
	<?php } ?>

	<div class="main">
		<?php if(!$finish){ ?>
			<div class="main__bg" style="background-image: url('images/main-start-bg.jpg');"></div>
		<?php } ?>

		<div class="container">
			<div class="main__inner">
				<div class="main__wrapper">
					<div class="main__logo _mob-loading-fade">
						<img src="images/logo.svg" alt="">
					</div>
					
					<ul class="main__nav _loading-fade">
						<li class="main__nav-item">
							<a href="#conditions" class="main__nav-link js-target-link">Призы</a>
						</li>
						<li class="main__nav-item">
							<a href="#" class="main__nav-link">Правила</a>
						</li>
					</ul>
					
					<div class="main__content _mob-loading-fade">
						<h1 class="main__title">
							<?php if(!$finish){ ?>
								<span class="main__title-t1">стань главным героем</span>
								<span class="main__title-t2">в ДОДО-игре</span>
							<?php } else { ?>
								<span class="main__title-t1">Ты&nbsp;спас Free&nbsp;City</span>
								<span class="main__title-t2">и&nbsp;превратился в&nbsp;настоящего героя!</span>
							<?php } ?>
						</h1>
					</div>
					
					<?php if(!$finish){ ?>
						<div class="main__start _loading-fade">
							<div class="main__start-overlay"></div>
							<div class="main__start-image">
								<img src="images/glasses.png" alt="">
							</div>
							<div class="main__start-btn-wrapper <?= $timer ? '_timer' : '' ?>">
								<?php if(!$timer){ ?>
									<button type="button" class="main__start-btn btn">
										<span>
											<span>СТАРТ</span>
										</span>
									</button>
								<?php } else { ?>
									<div class="main__timer start-timer" data-start="2021/08/13 15:00:00">
										<div class="main__timer-title">старт через</div>
										<div class="main__timer-content">
											<div class="main__timer-item">
												<span class="main__timer-value start-timer__days">0</span>
												<span class="main__timer-label">дн.</span>
											</div>
											<div class="main__timer-item">
												<span class="main__timer-value start-timer__time">00:00:00</span>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
					
					<?php if($finish){ ?>
						<div class="main__timer _loading-fade">
							<div class="main__timer-title">до результатов осталось</div>
							<div class="main__timer-content">
								<div class="main__timer-item">
									<span class="main__timer-value">23</span>
									<span class="main__timer-label">дн.</span>
								</div>
								<div class="main__timer-item">
									<span class="main__timer-value">07</span>
									<span class="main__timer-label">ч.</span>
								</div>
							</div>
						</div>
					<?php } ?>
					
					<?php if($finish){ ?>
						<div class="main__promo _loading-fade">
							<div class="main__promo-content">
								<div class="main__promo-title">Промокод на 15% скидку</div>
								<div class="main__promo-value">
									<div class="promo-copy">
										<div class="promo-copy__text">DODO2021PIZZA</div>
										<div class="promo-copy__tooltip">Текст скопирован!</div>
									</div>
								</div>
								<button type="button" class="main__promo-question-mark question-mark-btn" data-popup='{"text": "<p>Акция действует в&nbsp;ресторане, на&nbsp;доставку и&nbsp;самовывоз до&nbsp;5&nbsp;сентября 2021&nbsp;года, один&nbsp;раз.</p><p>Акция не&nbsp;действует с&nbsp;комбо и&nbsp;на&nbsp;дополнительные ингредиенты. Скидки по&nbsp;промокодам не&nbsp;суммируются. Предложение действует, пока есть&nbsp;товар в&nbsp;наличии.</p>", "theme": "blue", "size": "lg", "closeBtn": true}'></button>
							</div>
							<div class="main__promo-btn-wrapper">
								<a href="#" class="main__promo-btn btn">
									<span>
										<span>Заказать пиццу</span>
									</span>
								</a>
							</div>
						</div>
					<?php } ?>
					
					<span class="main__scroll _loading-fade"><span></span></span>
				</div>

				<?php if(!$finish){ ?>
					<div class="main__intro _loading-fade">
						<div class="main__intro-title">смотри Интро игры</div>
						<a href="#" class="main__intro-video">
							<div class="main__intro-cover" style="background-image: url('images/intro-thumb.jpg');"></div>
							<span class="main__intro-play"></span>
						</a>
					</div>
				<?php } ?>

				<?php if(!$finish && !$timer){ ?>
					<div class="main__mob-start _mob-loading-fade">
						<button type="button" class="main__start-btn btn">
							<span>
								<span>СТАРТ</span>
							</span>
						</button>
					</div>
				<?php } ?>

				<div class="main__results _loading-fade">
					<div class="main__results-title">Рейтинг игроков</div>
					<div class="main__results-table-wrapper">
						<div class="main__results-table">
							<div class="main__results-table-row">
								<div class="main__results-table-cell">1</div>
								<div class="main__results-table-cell">PrincessBANGBANG</div>
								<div class="main__results-table-cell">159</div>
								<div class="main__results-table-overlay"><span></span></div>
							</div>
							<div class="main__results-table-row">
								<div class="main__results-table-cell">2</div>
								<div class="main__results-table-cell">SweetDreams</div>
								<div class="main__results-table-cell">145</div>
								<div class="main__results-table-overlay"><span></span></div>
							</div>
							<div class="main__results-table-row">
								<div class="main__results-table-cell">3</div>
								<div class="main__results-table-cell">LuckyPizza</div>
								<div class="main__results-table-cell">133</div>
								<div class="main__results-table-overlay"><span></span></div>
							</div>
							<?php if($finish){ ?>
								<div class="main__results-table-row _user">
									<div class="main__results-table-cell">124</div>
									<div class="main__results-table-cell">Nickname</div>
									<div class="main__results-table-cell">68</div>
									<div class="main__results-table-overlay"><span></span></div>
								</div>
							<?php } ?>
						</div>
					</div>
					<div class="main__results-footer">
						<a href="ratings.php" class="main__results-more-link">Полный список</a>
					</div>
				</div>
				
				<div class="main__copyright _loading-fade">©2021 20th Century Studios</div>
			</div>
		</div>
	</div>

	<div class="conditions" id="conditions">
		<div class="container">
			<div class="conditions__header">
				<h2 class="conditions__title">призы и условия</h2>
				<div class="conditions__subtitle">
					<p>Акция начнется 1 августа и продлится до 30 августа включительно.</p>
					<p><span class="c-highlight">9 сентября 2021</span> определим счастливчиков, которые получат главные призы!</p>
				</div>
			</div>
			
			<div class="conditions__content">
				<div class="prizes-slider">
					<div class="prizes-slider__item">
						<div class="prize">
							<div class="prize__title"><span class="prize__title-num">1</span> место</div>
							<div class="prize__image">
								<img src="images/prize-5-1.png?1" class="is-active" alt="">
								<img src="images/prize-5-2.png?1" alt="">
								<img src="images/prize-5-3.png?1" alt="">
								<img src="images/prize-5-4.png?1" alt="">
							</div>
							<div class="prize__overlay"></div>
							<div class="prize__content">
								<div class="prize__content-inner">
									<span class="prize__name">Игровая приставка</span>
									<span class="prize__count">1 шт.</span>
								</div>
							</div>
						</div>
					</div>

					<div class="prizes-slider__item">
						<div class="prize">
							<div class="prize__title"><span class="prize__title-num">2-4</span> место</div>
							<div class="prize__image">
								<img src="images/prize-3-1.png?1" class="is-active" alt="">
								<img src="images/prize-3-2.png?1" alt="">
								<img src="images/prize-3-3.png?1" alt="">
								<img src="images/prize-3-4.png?1" alt="">
							</div>
							<div class="prize__overlay"></div>
							<div class="prize__content">
								<div class="prize__content-inner">
									<span class="prize__name">Проектор</span>
									<span class="prize__count">3 шт.</span>
								</div>
							</div>
						</div>
					</div>

					<div class="prizes-slider__item">
						<div class="prize">
							<div class="prize__title"><span class="prize__title-num">5-14</span> место</div>
							<div class="prize__image">
								<img src="images/prize-1-1.png?1" class="is-active" alt="">
								<img src="images/prize-1-2.png?1" alt="">
								<img src="images/prize-1-3.png?1" alt="">
								<img src="images/prize-1-4.png?1" alt="">
							</div>
							<div class="prize__overlay"></div>
							<div class="prize__content">
								<div class="prize__content-inner">
									<span class="prize__name">VR-очки</span>
									<span class="prize__count">10 шт.</span>
								</div>
							</div>
						</div>
					</div>

					<div class="prizes-slider__item">
						<div class="prize _hoodie">
							<div class="prize__title"><span class="prize__title-num">15-24</span> место</div>
							<div class="prize__image">
								<img src="images/prize-4-1.png?1" class="is-active" alt="">
								<img src="images/prize-4-2.png?1" alt="">
								<img src="images/prize-4-3.png?1" alt="">
								<img src="images/prize-4-4.png?1" alt="">
							</div>
							<div class="prize__overlay"></div>
							<div class="prize__content">
								<div class="prize__content-inner">
									<span class="prize__name">Худи</span>
									<span class="prize__count">10 шт.</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="conditions__text">
				<div class="conditions__text-item c-highlight">Всем участникам, прошедшим 3-ю миссию</div>
				<div class="conditions__text-item">Мясная пицца 25&nbsp;см в&nbsp;подарок при&nbsp;заказе от&nbsp;799&nbsp;₽</div>
			</div>

			<div class="conditions__footer">Изображения могут немного отличаться от представленных на сайте</div>
		</div>
	</div>
</div>

<?php if(!$finish){ ?>
	<div class="video-window" id="intro-window">
		<button type="button" class="video-window__close close-btn"></button>

	    <video controls playsinline webkit-playsinline>
	        <!-- <source src="video/intro.webm" type="video/webm" /> -->
	        <source src="video/intro.mp4" type="video/mp4" />
	    </video>
	</div>
<?php } ?>

<?php require 'blocks/footer-content.php'; ?>

<?php require 'blocks/login-window.php'; ?>

<?php require 'blocks/footer.php'; ?>